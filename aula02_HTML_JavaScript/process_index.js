//processamento dos dados de um formulario "index html"

//Acessando os elementos formulário do html
const formulario = document.getElementById("formulario1");

//adiciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function (evento)
{
    evento.preventDefault()/*previne o comportamento padrão do HTMOL em resposta a um evento*/

    //constante para tratar os dados recebidos dos elemntos do formulario
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value; 

    //Exibe um alerta com os dados coletados
    alert(`Nome: ${nome}    E-mail: ${email}`);
});