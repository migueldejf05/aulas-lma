const {createApp}= Vue;

createApp({
    data(){
        return{
            show: false,
            itens: ["Bola","Bolsa","Tenis"]
        };
    },//Fechamento data

    methods:{
        showItens: function(){
            this.show = !this.show;
        },//Fechamento showItens
    },//Fechamento methods
}).mount("#app");//Fechamento createApp