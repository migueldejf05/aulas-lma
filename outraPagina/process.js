const { createApp } = Vue;
createApp({
  data() {
    return {
      paginaInicial: "index.html",
    }; //Fim return
  }, //Fim data

  methods: {
    loadPage: function () {
      window.location.href = "pagina2.html";
    }, //Fim da loadpage
  }, //Fim methods
}).mount("#app");
 