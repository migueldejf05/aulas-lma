const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomindexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg',
            ],

            imagesInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
                'https://p2.trrsf.com/image/fget/cf/1200/1200/middle/images.terra.com/2022/10/25/953747685-6354be64ac72a.jpeg',
                'https://imageproxy.ifunny.co/resize:640x,quality:90x75/images/98cbc76a353565a73584541de1c53616d029beb6b69e495cce0c54426c920c4c_3.jpg'
            ],

        };//fim return
    },//fim data

    computed:{
        randomImage(){
            
            return this.imagensLocais[this.randomIndex];
        },
        randomImageInternet(){
            return this.imagesInternet[this.randomindexInternet];
        },

        
    },//fim do computed

    methods:{
        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);
            this.randomindexInternet = Math.floor(Math.random()*this.imagesInternet.length);
        }
    },//fim do methods

}).mount("#app");